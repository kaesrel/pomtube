# Pomtube

This application/microservice is based on this tutorial: https://github.com/bootstrapping-microservices

The application name is PomTube. It is an application for video streaming, uploading, and storing videos.

### Running Terraform Scripts
```bash
cd working/scripts
```

Initiate terraform 
```bash
terraform init
```

Build resources
```bash
terraform apply
```

Update resources
```bash
terraform apply -auto-approve
```

Remove resources
```bash
terraform destroy
```


