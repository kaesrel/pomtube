variable app_name {
    default = "Pomtube12"
}
variable location {
  default = "Southeast Asia"
}

variable admin_username {
  default = "linux_admin"
}

variable app_version {
  default = "1"
}

variable client_id {

}

variable client_secret {

}

variable storage_account_name {

}

variable storage_access_key {

}
