terraform {
    backend "azurerm" {
        resource_group_name  = "group1"
        storage_account_name = "kaesrelstore2"
        container_name       = "terraform"
        key                  = "kaesrelstore2.tfstate"
    }
}